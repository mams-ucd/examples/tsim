# Copyright  © University College Dublin 2021. All rights reserved, 
# The copyright to the computer software herein is the property of University College Dublin.  
# The software may be used, adapted, built on or copied only in accordance with the terms of the 
# Collaboration Agreement 2016 establishing CONSUS and must be kept confidential at all times.

from flask import Flask,request,Response,redirect, jsonify
from secrets import token_urlsafe

# from bson import ObjectId
# from pymongo import MongoClient
from os import environ
# from math import isnan
# from bson.json_util import dumps
# import py_eureka_client.eureka_client as eureka_client

# Constants
# MONGO_URL=environ.get('MONGO_URL', 'mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
HOST=environ.get('HOST', 'localhost')
PORT=environ.get('PORT', 8081)
# EUREKA_URL=environ.get('EUREKA_URL', "http://localhost:8761/")
BASE_URL='http://'+HOST+':'+str(PORT)+'/'
# DATABASE='HYPERMATS'
# COLLECTION='Links'

# Main Code
app = Flask(__name__)
# conn = MongoClient(MONGO_URL)

class Street:
    cells = []
    length = 0
    vehicles = {}

    def __init__(self, length):
        self.length = length
        self.cells = [None]*length

    def representation(self, path):
        return  {
            '@id':path,
            'length': self.length,
            'cells':self.cells,
            'vehicles':self.vehicles
        }
    
    def step(self):
        for i in range(self.length-1, -1, -1):
            if self.cells[i] is not None:
                print(f"processing: {i}={self.cells[i]}")
                vehicle = self.vehicles[self.cells[i]]
                
                # Move the vehicle forward upto vehicle.speed squares
                for j in range(1, vehicle['speed']+1):
                    if self.cells[i+j] is None:
                        print(f"transitioning to: {i+j}")
                        self.cells[i+j] = self.cells[i+j-1]
                        self.cells[i+j-1] = None
                    else:
                        # the vehicle could not complete all its steps,
                        # so reduce the speed to reflect this (auto stop)
                        vehicle['speed']=j
                        break

    
    def transmitState(self):
        for i in range(0, self.length):
            print(f"{i}={self.cells[i]}")

STREETS = {}
STREETS['s1']=Street(10)
STREETS['s1']

@app.route('/', methods=['GET'])
def index():
    return {
        '@id':request.base_url,
        '@context': {
            'hyper':'https://www.w3.org/2019/wot/hypermedia',
            'hmas':'http://tsim.astralanguage.com/2023/hypermassim/core',
            'tsim':'http://tsim.astralanguage.com/2023/hypermassim/tsim'
        },
        'links': [
            {
                'href': request.base_url+'time',
                'rel': 'hmas#time'
            }, {
                'href':request.base_url+'streets',
                'rel': 'tsim#streets'
            }
        ]
    }

@app.route('/time', methods=['PUT'])
def setTime():
    data = request.json
    print(f'{data}')

    for id in STREETS.keys():
        STREETS[id].step()

    for id in STREETS.keys():
        STREETS[id].transmitState()

    return Response(status=200)

@app.route('/streets',methods=['GET','POST'])
def streets():
    if request.method == 'GET':
        streets = []
        for id in STREETS.keys():
            streets.append({
                '@id':request.base_url+'/'+id,
            })
        return {
            '@id':request.base_url,
            'streets':streets
        }
    else:
        id = request.headers.get('Slug')
        if id is None:
            return Response("no slug specified",status=400)
        
        data = request.json
        STREETS[id] = Street(data['length'])
        return Response(status=201,headers={'Location':request.full_path+'/'+id})

@app.route('/streets/<id>',methods=['GET', 'POST'])
def getStreet(id):
    street = STREETS[id]
    if request.method == 'POST':
        vehicle_id = request.headers.get('Slug')
        if vehicle_id is None:
            return Response("slug required: normally this should be the agent name",status=400)
        
        data = request.json
        if street.cells[0] is None:
            street.cells[0]=vehicle_id
            token = token_urlsafe()
            data['bearer_token']=token
            street.vehicles[vehicle_id]=data
            response = jsonify({ 'bearer_token':token })
            response.status_code=201
            response.headers['Location'] = request.base_url+'/vehicles/'+vehicle_id
            response.headers['Content-Type']='application/json'
            return response
        else:
            return Response('initial street cell is occupied', status=409)
    else:
        return street.representation(request.base_url)
    
# The MAIN program
if __name__ == '__main__':
    app.debug=True
    app.run(host='0.0.0.0',port=PORT)
